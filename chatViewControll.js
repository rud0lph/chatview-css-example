$( document ).ready(function() {
  sizeManager();


  $('body').on('click','#chat-title',function(e) {
    console.log("toggled!");
    if (toggled == 0 ){
      toggled = 1;
    } else {
      toggled = 0;
    }

    sizeManager();
  })

});

var toggled = 0;

var sizeManager = function(){
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  if (toggled == 0 ){
    $('#chatView').addClass('chatView-m');
    $('#chatView').removeClass('chatView-lg');
    $('#chatView').removeClass('chatView-sm');
      $('#chat-title').addClass('chat-title-m');
      $('#chat-title').removeClass('chat-title-lg');
      $('#chat-title').removeClass('chat-title-sm');
    $('#chat-input').addClass('chat-input-m');
    $('#chat-input').removeClass('chat-input-lg');
    $('#chat-input').removeClass('chat-input-sm');
    $('#chat-content').addClass('chat-content-m');
    $('#chat-content').removeClass('chat-content-lg');
    $('#chat-content').removeClass('chat-content-sm');
    $('#chat-interior').addClass('chat-interior-m');
    $('#chat-interior').removeClass('chat-interior-lg');
    $('#chat-interior').removeClass('chat-interior-sm');

    $('#minimButton').source = '';
  } else {
    $('#chatView').addClass('chatView-lg');
    $('#chatView').removeClass('chatView-m');
    $('#chatView').removeClass('chatView-sm');
      $('#chat-title').addClass('chat-title-lg');
      $('#chat-title').removeClass('chat-title-m');
      $('#chat-title').removeClass('chat-title-sm');
    $('#chat-input').addClass('chat-input-lg');
    $('#chat-input').removeClass('chat-input-m');
    $('#chat-input').removeClass('chat-input-sm');
    $('#chat-content').addClass('chat-content-lg');
    $('#chat-content').removeClass('chat-content-m');
    $('#chat-content').removeClass('chat-content-sm');
    $('#chat-interior').addClass('chat-interior-lg');
    $('#chat-interior').removeClass('chat-interior-m');
    $('#chat-interior').removeClass('chat-interior-sm');

    $('#minimButton').source = 'res/minimize.png';
  }
} else {
  if (toggled == 0 ){
    $('#chatView').addClass('chatView-m');
    $('#chatView').removeClass('chatView-lg');
    $('#chatView').removeClass('chatView-sm');
      $('#chat-title').addClass('chat-title-m');
      $('#chat-title').removeClass('chat-title-lg');
      $('#chat-title').removeClass('chat-title-sm');
    $('#chat-input').addClass('chat-input-m');
    $('#chat-input').removeClass('chat-input-lg');
    $('#chat-input').removeClass('chat-input-sm');
    $('#chat-content').addClass('chat-content-m');
    $('#chat-content').removeClass('chat-content-lg');
    $('#chat-content').removeClass('chat-content-sm');
    $('#chat-interior').addClass('chat-interior-m');
    $('#chat-interior').removeClass('chat-interior-lg');
    $('#chat-interior').removeClass('chat-interior-sm');

    $('#minimButton').source = '';
  } else {
    $('#chatView').addClass('chatView-sm');
    $('#chatView').removeClass('chatView-lg');
    $('#chatView').removeClass('chatView-m');
      $('#chat-title').addClass('chat-title-sm');
      $('#chat-title').removeClass('chat-title-lg');
      $('#chat-title').removeClass('chat-title-m');
    $('#chat-input').addClass('chat-input-sm');
    $('#chat-input').removeClass('chat-input-lg');
    $('#chat-input').removeClass('chat-input-m');
    $('#chat-content').addClass('chat-content-sm');
    $('#chat-content').removeClass('chat-content-lg');
    $('#chat-content').removeClass('chat-content-m');
    $('#chat-interior').addClass('chat-interior-sm');
    $('#chat-interior').removeClass('chat-interior-lg');
    $('#chat-interior').removeClass('chat-interior-m');

    $('#minimButton').source = 'res/minimize.png';
  }
}
}
