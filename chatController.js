angular.module('alliChat', []).controller('chatController', ['$scope','$location', '$anchorScroll', '$sce', function($scope, $location, $anchorScroll, $sce) {

  $scope.messages = [];

    $scope.bottomStatus= true;

    $scope.recievedMessage = function(message){
      var recievedMsg = {
        message: $sce.trustAsHtml(message),
        type: "mess-in",
      }
      $scope.messages.push(recievedMsg);
      $scope.$apply();
      $scope.gotoBottom();
    }

    $scope.gotoBottom = function() {
      $location.hash('bottom');
      $anchorScroll();
    };


    $scope.submitMessage = function(message){
      if ($scope.bottomStatus == true) return $scope.gotoBottom();
      var data = {
        message: message,
        token: "token"
      };
      $scope.socket.emit('message', data, (data) => {
		  });
      var newMessage = {
        message: $sce.trustAsHtml(message),
        type: "mess-out"
      }
      $scope.entrie = null;
      $scope.messages.push(newMessage);
      $scope.gotoBottom();

    }

    var _parseData = function(message, urls) {
      var replaceUrlString = "##url##";
      for (urlIndex = 0; urlIndex < urls.length; urlIndex++) {
        var _urlHref = '<a target="_blank" href="' + urls[urlIndex] + '">' + urls[urlIndex] + '</a>';
        message = message.replace(replaceUrlString, _urlHref);
      }
      return message
    }

    var serverUrl = 'https://socket-buscador.herokuapp.com'
    $scope.socket = io(serverUrl);
	  $scope.socket.on('connect', function(){
      $scope.bottomStatus= false;
    });
		$scope.socket.on('respon', function(data) {
      var links = data.urls;
      var message = data.message;

      if (links.length > 0) {
        parsedMessage = _parseData(message, links)
        $scope.recievedMessage(parsedMessage)
      } else {
        $scope.recievedMessage(message);
      }
    });


	  $scope.socket.on('disconnect', function(){
      $scope.bottomStatus= true;
      var defaultDisconnectMessage = 'Lo sentimos, hubo un error en la conexión'
      $scope.recievedMessage(defaultDisconnectMessage)
    });

}])
.directive('scrollToBottom', function($timeout, $window) {
    return {
        scope: {
            scrollToBottom: "="
        },
        restrict: 'A',
        link: function(scope, element, attr) {
            scope.$watchCollection('scrollToBottom', function(newVal) {
                if (newVal) {
                    $timeout(function() {
                        element[0].scrollTop =  element[0].scrollHeight;
                    }, 0);
                }

            });
        }
    };
});
